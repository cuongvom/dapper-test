﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Dapper;
using Dapper.Contrib.Extensions;
using SelfStudyDapper.Models;
using Z.Dapper.Plus;

namespace SelfStudyDapper
{
    class Program
    {
        private static string _str = @"Data Source=HSSSC1PCL01580\SQLEXPRESS;Initial Catalog=self_study_dapper;Integrated Security=True;Persist Security Info=False;Pooling=False;MultipleActiveResultSets=False;Encrypt=False;TrustServerCertificate=False";
        private static IDbConnection _db = new SqlConnection(_str);

        static void Main(string[] args)
        {
            Console.WriteLine("---------------Hello Dapper!---------------");
            //InsertManyClass();
            //InsertSingleClass();
            //UpdateSingleClass();
            //UpdateManyClass();
            //DeleteSingleClass();
            //DeleteManyClass();
            //ExecuteReaderDataTable();
            //ExecuteScalarClass();
            //QueryAnonymousClass();
            //QueryStronglyTypeClass();
            //QueryMultiMapOneMany();
            //QueryMultiMapOneOne();
            //QueryMultiple();
            //ParameterExample();
            //QuerySingleStudent();
            //QueryFirstStudent();
            //ParamStringStudent();
            //TransactionStudent();
            //TransactionScopeExample();
            //DapperContribExample();
            //DapperPlusInsertOneMany();
            //DapperPlusUpdateOneMany();
            //DapperPlusDeleteOnemany();
            //DapperPlusDeleteSingleStudent();
            DapperPlusBulkMergeStudentExample();
            //while (true)
            //{
            //    Example();
            //    var input = Console.ReadLine();
            //    Console.WriteLine("Hello" + input);
            //}
            
            Console.ReadKey();
        }

        static async void Example()
        {
            // This method runs asynchronously.
            int t = await Task.Run(() => Allocate());
            Console.WriteLine("Compute: " + t);
        }

        static int Allocate()
        {
            // Compute total count of digits in strings.
            int size = 0;
            for (int z = 0; z < 100; z++)
            {
                for (int i = 0; i < 1000000; i++)
                {
                    string value = i.ToString();
                    size += value.Length;
                }
            }
            return size;
        }
    static void InsertSingleClass()
        {
            var str = @"INSERT INTO Class (ClassName) VALUES (@ClassName)";
            _db.Open();
            var affectedRow = _db.Execute(str,new {ClassName="ClassA"});
            _db.Close();
            Console.WriteLine("{0} row affected",affectedRow);
        }

        static void InsertManyClass()
        {
            var str = @"INSERT INTO Class (ClassName) VALUES (@ClassName)";
            _db.Open();
            var affectedRow = _db.Execute(str,new[] { 
                new {ClassName="ClassA"},
                new {ClassName="ClassB"},
                new {ClassName="ClassC"},
                new {ClassName="ClassD"},
            });
            _db.Close();
            Console.WriteLine("{0} row affected", affectedRow);
        }

        static void UpdateSingleClass()
        {
            var str = @"UPDATE Class SET ClassName=@ClassName
                      WHERE ClassID=@ClassID";
            _db.Open();
            var affectedRow = _db.Execute(str, new { Classname = "ClassAUpdate", ClassID = 1 });
            _db.Close();
            Console.WriteLine("{0} row affected", affectedRow);
        }

        static void UpdateManyClass()
        {
            var str = @"UPDATE Class SET Classname=@ClassName
                      WHERE ClassID=@ClassID";
            _db.Open();
            var affectedRow = _db.Execute(str, new[] {
                new {ClassName="ClassDUpdate",ClassID=4},
                new {ClassName="ClassBUpdate",ClassID=2}
            });
            _db.Close();
            Console.WriteLine("{0} row affected", affectedRow);
        }

        static void DeleteSingleClass()
        {
            var str = @"DELETE Class WHERE ClassID=@ClassID";
            _db.Open();
            var affectedRow = _db.Execute(str,new {ClassID=2});
            _db.Close();
            Console.WriteLine("{0} row affected", affectedRow);
        }

        static void DeleteManyClass()
        {
            var str = @"DELETE Class WHERE ClassID=@ClassID";
            _db.Open();
            var affectedRow = _db.Execute(str,new[] {
                new { ClassID = 1 },
                new { ClassID = 3 }
            });
            _db.Close();
            Console.WriteLine("{0} row affected", affectedRow);
        }

        static void ExecuteReaderDataTable()
        {
            var str = @"SELECT* FROM Class";
            _db.Open();
            var reader = _db.ExecuteReader(str);
            DataTable table = new DataTable();
            table.Load(reader);
            _db.Close();
            WriteTable(table);
        }

        static void ExecuteReaderDataSet()
        {
           
        }
        
        static void ExecuteScalarClass()
        {
            var str = @"SELECT * FROM Class";
            _db.Open();
            var result = _db.ExecuteScalar(str);
            _db.Close();
            Console.WriteLine(result);
        }

        static void QueryAnonymousClass()
        {
            var str = @"SELECT* FROM Class";
            _db.Open();
            var result = _db.Query(str).FirstOrDefault();
            _db.Close();
            Console.WriteLine(result);
        }

        static void QueryStronglyTypeClass()
        {
            var str = @"SELECT* FROM Class";
            _db.Open();
            var result = _db.Query<Class>(str).FirstOrDefault();
            _db.Close();

            Console.WriteLine(result.ClassName);
        }

        static List<Class> QueryMultiMapOneMany()
        {
            var str = @"SELECT * FROM Student AS S INNER JOIN Class AS C ON S.ClassID=C.ClassID WHERE C.ClassID=4;";
            var classDictionary = new Dictionary<int, Class>();
            _db.Open();
            var result = _db.Query<Student,Class,Class>(str,
                (stu, cla) =>
                {
                    Class classEntry;
                    if (!classDictionary.TryGetValue(cla.ClassID, out classEntry))
                    {
                        classEntry = cla;
                        classEntry.Students = new List<Student>();
                        classDictionary.Add(classEntry.ClassID, classEntry);
                    }

                    classEntry.Students.Add(stu);
                    return classEntry;
                },
                splitOn:"ClassID"
                ).Distinct().ToList();
            _db.Close();
            Console.WriteLine(result.Count);
            result.ForEach(cla => {
                cla.Students.ForEach(stu => Console.WriteLine("Class: {0}---Student: {1}",cla.ClassName,stu.StudentName));
            });
            return result;
        }

        static void QueryMultiMapOneOne()
        {
            var str = @"SELECT* FROM Student AS S INNER JOIN Class AS C ON S.ClassID=C.ClassID;";
            _db.Open();
            var result = _db.Query<Student, Class, Class>(str,
                (stu, cla) =>
                {
                    //cla.Student = stu;
                    return cla;
                },
                splitOn:"ClassID"
                ).Distinct().ToList();
            _db.Close();
            Console.WriteLine(result.Count);
        }

        static void QueryMultiple()
        {
            var str = @"SELECT* FROM Student; SELECT* FROM Class;";
            _db.Open();
            var result = _db.QueryMultiple(str);
            var stu = result.Read<Student>().First();
            var cl = result.Read<Class>().First();
            Console.WriteLine(stu.StudentName);
            Console.WriteLine(cl.ClassName);
        }

        static void QueryFirstStudent()
        {
            var str = @"SELECT* FROM STUDENT WHERE ClassID=@id";
            _db.Open();
            var result = _db.QueryFirst<Student>(str, new { id = 4 });
            _db.Close();
            Console.WriteLine(result.StudentName);
        }

        static void QuerySingleStudent()
        {
            var str = @"SELECT* FROM STUDENT WHERE StudentID=@id";
            _db.Open();
            var result = _db.QuerySingle<Student>(str,new { id=3});
            _db.Close();
            Console.WriteLine(result.StudentName);
        }

        static void ParameterExample()
        {
            var str = @"SELECT* FROM Student WHERE ClassID IN @ID";
            _db.Open();
            var result = _db.Query<Student>(str,new {ID=new[] {4,5} });
            _db.Close();
            foreach(var data in result)
            {
                Console.WriteLine(data.StudentName);
            }
        }

        static void ParamStringStudent()
        {
            var str = @"SELECT* FROM Student WHERE StudentName=@name";
            _db.Open();
            var result = _db.QueryAsync<Student>(str, new { name = new DbString { Value = "C", IsFixedLength = false, IsAnsi = true, Length = 10 } }).Result.First();
            _db.Close();
            Console.WriteLine(result.StudentName);
        }

        static void TransactionStudent()
        {
            var str = @"INSERT INTO Student (ClassID) VALUES (@ClassID)";
            _db.Open();
            using (var trans = _db.BeginTransaction())
            {
                var insert1 = _db.Execute(str,new { ClassID = 4});
                var insert2 = _db.Execute(str, new { ClassID = "a" });
                Console.WriteLine(insert1);
                Console.WriteLine(insert2);
                //trans.Commit();
            }
                
        }

        static void TransactionScopeExample()
        {
            using (var trans=new TransactionScope())
            {
                try
                {
                    var str1 = @"INSERT INTO Student (StudentName,ClassID) VALUES (@StudentName,@ClassID)";
                    _db.Open();
                    _db.Execute(str1, new { StudentName = "test transactionscope1", ClassID = 8 });
                    _db.Close();
                    trans.Complete();
                }
                catch (Exception ex)
                {
                    trans.Dispose();
                    Console.WriteLine(ex);
                }
            }
        }

        static void DapperContribExample()
        {
            //_db.Open();
            var x=_db.Get<Student>(3);
            Console.WriteLine(x.StudentName);
        }

        static void DapperPlusInsertOneMany()
        {
            DapperPlusManager.Entity<Class>().Table("Class").Identity(x=>x.ClassID);
            DapperPlusManager.Entity<Student>().Table("Student").Identity(x=>x.StudentID);
            var listClass = new List<Class>() {
                new Class(){ClassName="Plus3",Students=new List<Student>()
                {
                    new Student(){StudentName="Plus3Student1"},
                    new Student(){StudentName="Plus3Student2"}
                }
                },
                new Class(){ClassName="Plus4",Students=new List<Student>()
                {
                    new Student(){StudentName="Plus4Student1"},
                    new Student(){StudentName="Plus4Student2"}
                }
                }
            };
            try
            {
                _db.Open();
                _db.BulkInsert(listClass).ThenForEach(x => x.Students.ForEach(y => y.ClassID = x.ClassID)).ThenBulkInsert(x => x.Students);
                _db.Close();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        static void DapperPlusUpdateOneMany()
        {
            var listClass = QueryMultiMapOneMany();
            listClass.ForEach(x=> {
                x.ClassName = "BulkUpdate1";
                x.Students.ForEach(t => t.StudentName = "BulkUpdateStudent1");
            });
            _db.Open();
            _db.BulkUpdate(listClass,x=>x.Students);
            _db.Close();
        }

        static void DapperPlusDeleteOnemany()
        {
            var listClass = QueryMultiMapOneMany();
            _db.Open();
            _db.BulkDelete(listClass.SelectMany(x => x.Students)).BulkDelete(listClass);
            _db.Close();
            Console.WriteLine("asdsad"+1);
        }

        static void DapperPlusDeleteSingleStudent()
        {
            _db.Open();
            var student = _db.Get<Student>(38); ;
            _db.BulkDelete(student);
            _db.Close();
        }

        static void DapperPlusBulkMergeStudentExample()
        {
            var str = @"SELECT* FROM Student";
            _db.Open();
            var listStudent = _db.Query<Student>(str).ToList();
            for(int i = 5; i <= 10; i++)
            {
                listStudent.Add(new Student() { 
                    StudentName="Student bulk merge"+i,
                    ClassID=7
                });
            }
            listStudent.First().StudentName = "Student test update merge";
            _db.BulkMerge(listStudent);
            _db.Close();
        }

        static void WriteTable(DataTable table)
        {
            foreach(DataRow datarow in table.Rows)
            {
                foreach(DataColumn dataColumn in table.Columns)
                {
                    Console.WriteLine(String.Format("{0}={1}",dataColumn.ColumnName,datarow[dataColumn]));
                }
                Console.WriteLine("-----------------");
            }
        }
    }
}
