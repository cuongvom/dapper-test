﻿using Dapper.Contrib.Extensions;

namespace SelfStudyDapper.Models
{
    [Table("Student")]
    public class Student
    {
        [Key]
        public int StudentID { get; set; }
        public string StudentName { get; set; }
        public int ClassID { get; set; }
        public Class Classes { get; set; }
    }
}
