﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SelfStudyDapper.Models
{
    public class Class
    {
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public List<Student> Students { get; set; }
    }
}
